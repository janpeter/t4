# README #

Implementation of task 4 for Software Architecture

### What does it need to run it? ###

* Install Node Version Manager
* `nvm install 5.4`
* `nvm use 5.4`
* `npm install sqlite3`
* `npm install -g gulp`
* `gulp`

Then you can reach the application via `http://localhost:3000/`