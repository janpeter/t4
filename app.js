/// <reference path="typings/node/node.d.ts"/>

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var restaurants = require('./routes/restaurants');
var stops = require('./routes/stops');
var route = require('./routes/route');

var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
server.listen(4200);

var env = process.env.NODE_ENV || 'development';
app.locals.ENV = env;
app.locals.ENV_DEVELOPMENT = env == 'development';
app.locals.TASK = 3;
// Buffer calls for task 4
app.locals.BUFFER = 2;

app.locals.caller = [];

app.locals.addCall = function(call) {
    if (app.locals.TASK !== 4) {
        call.fn.apply(this, call.args);
    } else {
        app.locals.caller.push(call);
        app.locals.check();
    }
};

app.locals.check = function() {
    if (app.locals.caller.length > app.locals.BUFFER) {
        app.locals.caller.forEach(function(c) {
            if(c.fn !== undefined) {
                c.fn.apply(this, c.args);
            }
        });
        app.locals.caller = [];
    }
};

// view engine setup

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// app.use(favicon(__dirname + '/public/img/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/restaurants', restaurants);
app.use('/stops', stops);
app.use('/route', route);

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace

if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err,
            title: 'error'
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {},
        title: 'error'
    });
});

var changes = [];

io.on('connection', function (socket) {
  socket.on('update', function (data) {
    changes.push(data);
    if (app.locals.caller.length > app.locals.BUFFER - 1 || app.locals.TASK !== 4) {
      socket.broadcast.emit('update', changes);
      changes = [];
    }
  });
});


module.exports = app;
