/* global Filter */
/// <reference path="../../typings/angularjs/angular.d.ts"/>

var app = angular.module("stopBrowser", ['ngMaterial', 'ngRoute']);

var socket = io.connect('http://localhost:4200');

app.filter('unique', function() {
   return function(collection, keyname) {
      var output = [], keys = [];
      angular.forEach(collection, function(item) {
          var key = item[keyname];
          if(keys.indexOf(key) === -1) {
              keys.push(key);
              output.push(item);
          }
      });
      return output;
   };
});

app.factory('Route', ['$http', function($http) {
  return {
    check: function(stops, handler) {
      $http.post('/route/check', stops).then(function(response) {
        if (!response.data.error) {
          handler(response.data.stops);
        }
      });
    },
    addNewRoute: function(name, stops) {
      $http.put('/route', {name: name, stops: stops});
    },
    getStops: function(route, fn) {
      $http.get('/route/' + route.id).then(function(response) {
        fn(response.data);
      });
    },
    update: function(id, name, stops, fn) {
      $http.post('/route', { id: id, name: name, stops: stops });
    }
  };
}]);

app.factory('Stops', ['$http', function($http) {
  return {
    getAllStops: function(handler) {
      $http.get('/stops').then(function(response) {
        if (!response.data.error) {
          handler(response.data.rows);
        }
      });
    },
    
    save: function(stop, handler) {
      socket.emit('update', { id: stop.id, name: stop.name, lat: stop.lat, lon: stop.lon });
      $http.post('/stops', stop).then(function(response) {
        handler(response.data.error);
      });
    }
  };
}]);

app.factory('Restaurants', ['$http', function($http) {
  return {
    nearby: function(distance, stop, handler) {
      $http.post('/restaurants/nearby', { distance: distance, stop: stop }).then(function(response) {
        if (!response.data.error) {
          handler(response.data.restaurants);
        }
      });
    }
  };
}]);

app.factory('routeService', function() {
  return {
    restaurants: [],
    stops: [],
    routes: []
  };
});

app.config(function($routeProvider) {
  $routeProvider
  .when('/', {
      templateUrl : 'stops.html',
      controller  : 'StopsCtrl'
  })
  .when('/routes', {
      templateUrl : 'routes.html',
      controller  : 'RouteCtrl'
  })
  .when('/stop/edit', {
      templateUrl : 'editStop.html',
      controller  : 'StopCtrl'
  })
  .when('/restaurants', {
      templateUrl : 'restaurants.html',
      controller  : 'RestaurantsCtrl'
  })
  .when('/route/new', {
      templateUrl : 'newRoute.html',
      controller  : 'RouteCtrl'
  })
  .when('/route/edit', {
      templateUrl : 'editRoute.html',
      controller  : 'EditRouteCtrl'
  });
});

app.controller('StopCtrl', function($scope, $location, routeService, Stops) {
  if (routeService.stops.length == 0) {
    $location.path('/');
  }
  $scope.stop = null;
  if (routeService.stops.length > 0) {
    $scope.stop = routeService.stops[0];
  }
  
  $scope.changeView = function(view){
    $location.path(view);
  };
  
  $scope.save = function() {
    Stops.save($scope.stop, function() {
      $scope.changeView('/');
    });
  };
});

app.controller('RestaurantsCtrl', function($scope, $location, routeService, Restaurants) {
  $scope.routes = routeService.routes;
  $scope.stops = routeService.stops;
  $scope.restaurants = routeService.restaurants;
  
  $scope.changeView = function(view){
    $location.path(view);
  };
});

app.controller('EditRouteCtrl', function($scope, $location, routeService, Route, Stops) {
  if (routeService.routes.length == 0) {
    $location.path('/');
  }
  
  $scope.route = routeService.routes[0];
  
  if ($scope.route.name !== undefined) {
    $scope.route.name = $scope.route.name.split(':')[0];
  }
  $scope.selected = routeService.stops;
  $scope.allStops = [];
  $scope.stops = [];
  $scope.uniqueElements = 0;
  $scope.totalElements = 50;
  $scope.canLoadMore = false;
  $scope.distance = 100;
  $scope.unique = true;
  
  function calcUnique(stops) {
    var t = [];
    stops.forEach(function(element) {
      var isIn = false;
      t.forEach(function(l) {
        if(element.name === l.name) {
          isIn = true;
        }
      });
      if(!isIn) {
        t.push(element);
      }
    });
    $scope.uniqueElements = t.length;
    $scope.canLoadMore = $scope.uniqueElements > $scope.totalElements;
  }
  
  Stops.getAllStops(function(stops) {
    $scope.allStops = stops;
    calcUnique(stops);
    $scope.selected.forEach(function(e) {
      var rm = -1;
      stops.forEach(function(s, i) {
        if (s.id === e.id) {
          rm = i;
        }
      });
      
      if (rm >= 0) {
        stops.splice(rm, 1);
        stops.unshift(e);
      }
    });
    
    $scope.stops = stops;
    
  });
  
  $scope.save = function() {
    Route.update($scope.route.id, $scope.route.name + ': ' + $scope.selected[0].name + ' => ' + $scope.selected[$scope.selected.length - 1].name, $scope.selected);
    $scope.changeView('/');
  };
  
  $scope.loadMore = function() {
    $scope.totalElements += 50;
    $scope.canLoadMore = $scope.uniqueElements > $scope.totalElements;    
  };
  
  $scope.filter = function() {
    $scope.totalElements = 50;
    var filter = new Filter.Filter();
    
    $scope.stops = filter.each($scope.allStops, [
      new Filter.NameFilter($scope.stop.name)
    ]);
    calcUnique($scope.stops);
  };
  
  $scope.changeView = function(view){
    $location.path(view);
  };
  
  $scope.up = function(i, stop) {
    $scope.selected.splice(i, 1);
    $scope.selected.splice(i-1, 0, stop);
  };
  
  $scope.down = function(i, stop) {
    $scope.selected.splice(i, 1);
    $scope.selected.splice(i+1, 0, stop);
  };
  
  $scope.create = function() {
    Route.addNewRoute($scope.routeName + ': ' + $scope.selected[0].name + ' => ' + $scope.selected[$scope.selected.length - 1].name, $scope.selected);
    $scope.changeView('/');
  };
  
  $scope.select = function(stop) {
    if (stop.selected) {
      $scope.selected.push(stop);
    } else {
      var rm = -1;
      $scope.selected.forEach(function(element, i) {
        if(element.id === stop.id) {
          rm = i;
        }
      });
      $scope.selected.splice(rm, 1);
    }
  };
});

app.controller('RouteCtrl', function($scope, $location, routeService, Route) {
  $scope.routes = routeService.routes;
  $scope.stops = routeService.stops;
  
  $scope.changeView = function(view){
    $location.path(view);
  };
  
  $scope.edit = function(route) {
    Route.getStops(route, function(data) {
      routeService.routes = [route];
      routeService.stops = data.rows.map(function(e) {
        e.selected = true;
        return e;
      });
      $scope.changeView('/route/edit');
    });
  };
  
  $scope.up = function(i, stop) {
    $scope.stops.splice(i, 1);
    $scope.stops.splice(i-1, 0, stop);
  };
  
  $scope.down = function(i, stop) {
    $scope.stops.splice(i, 1);
    $scope.stops.splice(i+1, 0, stop);
  };
  
  $scope.create = function() {
    Route.addNewRoute($scope.routeName + ': ' + $scope.stops[0].name + ' => ' + $scope.stops[$scope.stops.length - 1].name, $scope.stops);
    $scope.changeView('/');
  };
  
  $scope.select = function(stop) {
    var rm = -1;
    $scope.stops.forEach(function(element, i) {
      if(element.id === stop.id) {
        rm = i;
      }
    });
    $scope.stops.splice(rm, 1);
  };
});

app.controller('StopsCtrl', function ($scope, $mdDialog, $mdMedia,
    $location, routeService, Stops, Route, Restaurants) {
  $scope.allStops = [];
  $scope.stops = [];
  $scope.selected = [];
  $scope.uniqueElements = 0;
  $scope.totalElements = 50;
  $scope.canLoadMore = false;
  $scope.searchText = '';
  $scope.distance = 100;
  $scope.unique = true;
  
  
  socket.on('update', function (data) {
    $scope.allStops.forEach(function(e) {
      data.forEach(function(d) {
        if (e.id == d.id) {
          e.name = d.name;
          e.lat = d.lat;
          e.lon = d.lon;
        }
      });
    });
    $scope.$apply();
  });
  
  function calcUnique(stops) {
    var t = [];
    stops.forEach(function(element) {
      var isIn = false;
      t.forEach(function(l) {
        if(element.name === l.name) {
          isIn = true;
        }
      });
      if(!isIn) {
        t.push(element);
      }
    });
    $scope.uniqueElements = t.length;
    $scope.canLoadMore = $scope.uniqueElements > $scope.totalElements;
  }
  
  Stops.getAllStops(function(stops) {
    $scope.allStops = stops;
    calcUnique(stops);
    $scope.stops = stops;
  });
  
  $scope.loadMore = function() {
    $scope.totalElements += 50;
    $scope.canLoadMore = $scope.uniqueElements > $scope.totalElements;    
  };
  
  $scope.filter = function() {
    $scope.totalElements = 50;
    var filter = new Filter.Filter();
    
    $scope.stops = filter.each($scope.allStops, [
      new Filter.NameFilter($scope.stop.name),
      new Filter.LatFilter($scope.latLimit1),
      new Filter.LatFilter($scope.latLimit2),
      new Filter.LonFilter($scope.lonLimit1),
      new Filter.LonFilter($scope.lonLimit2)
    ]);
    calcUnique($scope.stops);
  };
  
  $scope.edit = function() {
    routeService.stops = $scope.selected;
    $scope.changeView('stop/edit');
  };
  
  $scope.changeView = function(view){
    $location.path(view); // path not hash
  };
  
  $scope.connected = function() {
    Route.check($scope.selected, function(rows) {
      routeService.stops = $scope.selected;
      routeService.routes = rows;
      $scope.changeView('routes');
    });
  };
  
  $scope.nearby = function() {
    Restaurants.nearby($scope.distance, $scope.selected[0], function(rows) {
      routeService.stops = $scope.selected;
      routeService.restaurants = rows;
      $scope.changeView('restaurants');
    });
  };
  
  $scope.newRoute = function() {
    routeService.stops = $scope.selected;
    $scope.changeView('route/new');
  };
  
  $scope.select = function(stop) {
    if (stop.selected) {
      $scope.selected.push(stop);
    } else {
      var rm = -1;
      $scope.selected.forEach(function(element, i) {
        if(element.id === stop.id) {
          rm = i;
        }
      });
      $scope.selected.splice(rm, 1);
    }
  };
});