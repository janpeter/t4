var Filter;
(function (Filter_1) {
    function Compare(val, match) {
        if (typeof match !== 'undefined' && match.length > 0) {
            if (match.charAt(0) === '<') {
                match = match.substr(1, match.length);
                return val < +match;
            }
            else if (match.charAt(0) === '>') {
                match = match.substr(1, match.length);
                return val > +match;
            }
            else {
                return val === match;
            }
        }
        return true;
    }
    var Filter = (function () {
        function Filter() {
        }
        Filter.prototype.each = function (stops, filters) {
            filters.forEach(function (fn) {
                stops = fn.filter(stops);
            });
            return stops;
        };
        return Filter;
    })();
    Filter_1.Filter = Filter;
    var IdFilter = (function () {
        function IdFilter(value) {
            this.value = value;
        }
        IdFilter.prototype.filter = function (stops) {
            var result = [];
            stops.forEach(function (element) {
                if (element.id === this.value) {
                    result.push(element);
                }
            }, this);
            return result;
        };
        return IdFilter;
    })();
    Filter_1.IdFilter = IdFilter;
    var LatFilter = (function () {
        function LatFilter(value) {
            this.value = value;
        }
        LatFilter.prototype.filter = function (stops) {
            var result = [];
            stops.forEach(function (element) {
                if (Compare(element.lat, this.value)) {
                    result.push(element);
                }
            }, this);
            return result;
        };
        return LatFilter;
    })();
    Filter_1.LatFilter = LatFilter;
    var LonFilter = (function () {
        function LonFilter(value) {
            this.value = value;
        }
        LonFilter.prototype.filter = function (stops) {
            var result = [];
            stops.forEach(function (element) {
                if (Compare(element.lon, this.value)) {
                    result.push(element);
                }
            }, this);
            return result;
        };
        return LonFilter;
    })();
    Filter_1.LonFilter = LonFilter;
    var NameFilter = (function () {
        function NameFilter(value) {
            this.value = value;
        }
        NameFilter.prototype.filter = function (stops) {
            var result = [];
            stops.forEach(function (element) {
                if (element.name.indexOf(this.value) >= 0) {
                    result.push(element);
                }
            }, this);
            return result;
        };
        return NameFilter;
    })();
    Filter_1.NameFilter = NameFilter;
})(Filter || (Filter = {}));
