/// <reference path="../typings/node/node.d.ts"/>

var express = require('express');
var path = require('path');
var router = express.Router();

var stops = require('stops');

router.get('/', function(req, res) {
  res.sendfile(path.join(__dirname, '../public/app.html'));
});

module.exports = router;
