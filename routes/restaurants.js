var express = require('express');
var router = express.Router();

var restaurants = require('restaurants');
var stops = require('stops');

router.post('/nearby', function(req, res) {
    if (req.body.distance === undefined) {
        res.render({ error: true, message: 'You need to enter a distance' });
    } else {
        var stop = new stops.Stop(req.body.stop.id, req.body.stop.lon, req.body.stop.lat, req.body.stop.name);
        restaurants.getNearbyRestaurants(stop, req.body.distance, function(rows) {
            res.send({ error: false, restaurants: rows });
        });
    }
});

module.exports = router;
