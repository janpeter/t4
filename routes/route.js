var express = require('express');
var router = express.Router();

var stops = require('stops');
var route = require('route');
var _ = require('underscore');

/*
 * Get Route by start and stop node name
 *
 */
router.get('/:start/:stop', function(req, res) {
    if (req.params.start !== undefined && req.params.stop !== undefined) {
        route.getRoute(req.params.start, req.params.stop, function(rows) {
            route.getRouteNames(rows, function(r) {
                res.send({ error: false, r: r });
            });
        });
    } else {
        res.send({ error: true });
    }
});

/*
 * Get all stops for given route id
 *
 */
router.get('/:id', function(req, res) {
    if (req.params.id !== undefined) {
        route.getStops(req.params.id, function(rows) {
            res.send({ error: false, rows: rows});
        });
    } else {
        res.send({ error: true });
    }
});

/*
 * Creates new line by name and stop list
 *
 */
router.put('/', function(req, res) {
    if (req.body.name !== undefined && req.body.stops !== undefined
        && req.body.stops.length !== undefined && req.body.stops.length >= 2) {
        
        req.app.locals.addCall({
            fn: route.addNewRoute,
            args: [req.body.name, req.body.stops]
        });
        res.send({ error: false, name: req.body.name, stops: req.body.stops });
    } else {
        res.send({ error: true, message: 'You must enter a name and check at least 2 stops' });
    }
});

/*
 * Updates line by id
 *
 */
router.post('/', function(req, res) {
    if (req.body.id !== undefined && req.body.name !== undefined && req.body.stops !== undefined
        && req.body.stops.length !== undefined && req.body.stops.length >= 2) {
        
        req.app.locals.addCall({
            fn: route.update,
            args: [req.body.id, req.body.name, req.body.stops]
        });
        res.send({ error: false, name: req.body.name, stops: req.body.stops });
    } else {
        res.send({ error: true, message: 'You must enter a name and check at least 2 stops' });
    }
});

/*
 * Check for a connection between 2 stops
 *
 */
router.post('/check', function(req, res) {
    if (req.body.length !== 2) {
        res.send({ error: true, message: 'You can only check 2 stops'});
    } else {
        route.getRoute(req.body[0].name, req.body[1].name, function(rows) {
            route.getRouteNames(rows, function(r) {
                var distinct = [];
                r.forEach(function(e) {
                    if (!_.find(distinct, function(d) { return d.name.split(':')[0] === e.name.split(':')[0] })) {
                        distinct[distinct.length] = e;
                    }
                });
                
                res.send({
                    error: false,
                    stops: distinct
                });
            });
        });
    }
});

module.exports = router;
