var express = require('express');
var router = express.Router();

var stops = require('stops');

router.get('/', function(req, res) {
  stops.getAllStops(function(rows) {
    res.send({ error: false, rows: rows });
  });
});

router.post('/', function(req, res) {
  if (req.body.id !== undefined && req.body.lon !== undefined && req.body.lat !== undefined && req.body.name !== undefined) {
    var data = req.body;
    var stop = new stops.Stop(data.id, data.lon, data.lat, data.name);
    req.app.locals.addCall({
      fn: stops.update,
      args: [stop]
    });
    res.send({ error: false });
  } else {
    res.send({ error: true });
  }
});

module.exports = router;
